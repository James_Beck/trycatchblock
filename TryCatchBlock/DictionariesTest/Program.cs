﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DictionariesTest
{
    class Program
    {
        static void Main(string[] args)
        {
            //Store variables
            var input = "";

            //Program Start. Explanation to the user
            Console.WriteLine("Hi there user, go ahead and enter a number.");
            try
            {
                input = Console.ReadLine();
                int.Parse(input);
                Console.WriteLine("Great, you typed in a the number {0}.", input);
            }
            catch
            {
                input = "0";
                int.Parse(input);
                Console.WriteLine("Oh no, you didn't type in a number, so now the number input is equal to {0}.", input);
            }
        }
    }
}
